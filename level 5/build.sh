#!/bin/bash

# Remove old build
docker rm nexus.docker.passengersfriend.com/level5-app

# Build an image from a Dockerfile
# -t/--tag = Name and optionally a tag in the 'custom-repo/name:tag' format
# use URL to custom docker Repository in front of image name
# . = look for Dockerfile in current directory
docker build -t nexus.docker.passengersfriend.com/level5-app .

