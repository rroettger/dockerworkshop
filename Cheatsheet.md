# Docker Cheatsheet
## Building
Build an image from the Dockerfile in the current directory and tag the image
```
docker build -t myimage:1.0 .
```
List all images that are locally stored with the Docker Engine
```
docker image ls
```
Delete an image from the local image store
```
docker image rm alpine:3.4
docker rmi alpine:3.4
```
Delete all images from the local image store
```
docker image rm $(docker image ls -q)
docker rmi $(docker image ls -q)
```
## Sharing
Pull an image from a registry
```
docker pull myimage:1.0
```
Retag a local image with a new image name and tag
```
docker tag myimage:1.0 myregistry.tld/myimage:2.0
```
Login to custom repository
```
docker login myregistry.tld
```
Push an image to a registry
```
docker push myregistry.tld/myimage:2.0 
```
Save an image to a tar file
```
docker save myimage:1.0 > myimage.tar
```
Load an image to a tar file
```
docker load -i myimage.tar
```

## Running
Run a container from the Alpine version 3.9 image, name the running container “web” and expose port 5000 externally,
mapped to port 80 inside the container.
```
docker run --name web -p 5000:80 alpine:3.9
```
Run and a map a port
```
docker run -p 8080:80 IMAGE
```
Run and map a local directory into the container
```
docker run -v HOSTDIR:TARGETDIR IMAGE
```
Run and change the entrypoint
```
docker run -it --entrypoint EXECUTABLE IMAGE
```

## Managing
List the running containers (add --all/-a to include stopped containers)
```
docker ps
```
Stop a running container through SIGTERM
```
docker stop web
```
Stop a running container through SIGKILL
```
docker kill web
```
Stop all running containers
```
docker stop $(docker ps -aq)
```
Delete all running and stopped containers
```
docker rm -f $(docker ps -aq)
```
Copy files between Container and Host
```
docker cp CONTAINER:SOURCE TARGET
docker cp TARGET CONTAINER:SOURCE
docker cp web:/index.html index.html
```
Start a shell inside a running container
```
docker exec -it CONTAINER EXECUTABLE
docker exec -it web bash
```

## Info & Stats
Show stats of running containers, like load, memory etc.
```
docker stats
```
Show processes of a running container
```
docker top web
```
Print the last 100 lines of a container’s logs and follow
```
docker logs --tail 100 -f web
```
List all networks
```
docker network ls
```
List all volumes
```
docker volume ls
```
Get detailed information
```
docker inspect web
```
Show modified files in a container
```
docker diff web
```
Show mapped ports of a container
```
docker port web
```


## Utility
Clean all unused data of docker
```
docker system prune
```
