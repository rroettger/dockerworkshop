#!/bin/bash

# Remove old build
docker rm level2-app-container

# Start container with previously build image
# -it = Open pseudo TTY-console for viewing output of the container
# --name = Name for the container
# -p = Use following ports 8080 is externally e.g. localhost:8080 and 80 is internally
# -v = Use volume command to mount local folder into docker container $LOCAL_PATH:$CONTAINER_PATH
# httpd = Use Image from dockerhub
docker run -it --name level2-app-container -p 8080:80 -v "$(pwd)"/www2:/usr/local/apache2/htdocs httpd
