#!/bin/bash

# Remove old build
docker rm level1-app-container

# Build an image from a Dockerfile
# -t/--tag = Name and optionally a tag in the 'name:tag' format
# . = look for Dockerfile in current directory
docker build -t level1-app .

# Start container with previously build image
# -it = Open pseudo TTY-console for viewing output of the container
# --name = Name for the container
# -p = Use following ports 8080 is externally e.g. localhost:8080 and 80 is internally
# level1-app = Previously built image
docker run -it --name level1-app-container -p 8080:80 level1-app
