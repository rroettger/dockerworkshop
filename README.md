# Level 1
Basic docker example extending an pre-built apache2 image to deliver an own simple webpage.
# Level 2
Extend Level 1 with the use of volumes and show the difference between copying at build stage and mounting volumes at runtime.
# Level 3
Recreate Example from 1 without a Dockerfile. What are the advantages and what are the disadvantages?
# Level 4
Example with docker-compose, a dummy database and use of Build/Environment Arguments
# Level 5
Push to custom Repo
## Prerequisite
docker login nexus.docker.passengersfriend.com

