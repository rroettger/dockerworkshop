#!/bin/bash

# Remove old build
docker rm level2-app-container

# Build an image from a Dockerfile
# -t/--tag = Name and optionally a tag in the 'name:tag' format
# . = look for Dockerfile in current directory
docker build -t level2-app .

# Start container with previously build image
# -it = Open pseudo TTY-console for viewing output of the container
# --name = Name for the container
# -p = Use following ports 8080 is externally e.g. localhost:8080 and 80 is internally
# -v = Use volume command to mount local folder into docker container $LOCAL_PATH:$CONTAINER_PATH
# level2-app = Previously built image
docker run -it --name level2-app-container -p 8080:80 -v "$(pwd)"/www2:/usr/local/apache2/htdocs level2-app
